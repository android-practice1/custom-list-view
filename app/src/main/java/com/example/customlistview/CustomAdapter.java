package com.example.customlistview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class CustomAdapter extends BaseAdapter {

    String[] N; // name of os
    int[] I; // os image
    Context ct; // constructor
    public static LayoutInflater inflater = null; // no idea what the fuck is this

    // this is a constructor
    public CustomAdapter(MainActivity mainActivity, String[] OSname, int[] OSimage) {
        N = OSname;
        I = OSimage;
        ct = mainActivity;
        inflater = (LayoutInflater) ct.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // adding name size
        return N.length;
    }

    @Override
    public Object getItem(int position) {
        // return its position
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    // creating a class as a holder
    public class MyHolder {
        TextView textView;
        ImageView imageView;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        // creating a object of holder class
        MyHolder myHolderObject = new MyHolder();

        // inflater view with object
        View viewObject = inflater.inflate(R.layout.custom_layout, null);

        myHolderObject.textView = (TextView) viewObject.findViewById(R.id.textview_id);
        myHolderObject.imageView = (ImageView) viewObject.findViewById(R.id.imageview_id);

        myHolderObject.textView.setText(N[position]);
        myHolderObject.imageView.setImageResource(I[position]);

        // onclick listener
        viewObject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // toast
                Toast.makeText(ct, "clicked on " + N[position], Toast.LENGTH_LONG).show();
            }
        });

        return viewObject;
    }

}