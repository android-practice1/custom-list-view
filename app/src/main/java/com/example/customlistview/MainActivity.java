package com.example.customlistview;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // ListView from (main activity)layout
        ListView mylist = findViewById(R.id.myCustomListViewLayout);

        // Array of Item
        String[] NameOFOperatingSystem = {"Android OS","iOS","Windows OS","Java","BlackBerry OS",};

        // locating icon on /res/drwable/
        int[] ImageOFOperatingSystem = {R.drawable.android, R.drawable.apple, R.drawable.windows, R.drawable.java, R.drawable.blackberry};

        // Custom Adapter what we've created before
        CustomAdapter myAdapterObject = new CustomAdapter(this, NameOFOperatingSystem, ImageOFOperatingSystem);

        // calling adapter
        mylist.setAdapter(myAdapterObject);
    }
}